# Data Structure

## Models

+ meetings
+ Users

## Structure

### Users

```javascript
email : {
  validate: true,
  name : 'Estefania Guardado',
  deviceToken: {
    413112341b1341413b14134 : true
  },
  country: 'MX',
  timeZone: -6,
  photo: '14241341.png',
  meeting: {
    1: {
      date: 1448008691974,
      active: true,
      name: 'Meeting 1'
    }
  }
}
```

### Meeting

```javascript
1: {
  detail: {
    name: 'Meeting 1',
    date: 1448008691974,
    duration: 360,
    creator: 'Estefania Guardado'
  },
  guests: {[
    1: {
      name: 'Estefania Guardado',
      photo: '14241341.png',
      email: 'fachinacg@gmail.com'
    },
    2: {
      name: 'Luis Alejandro Rangel',
      photo: '14241323.png',
      email: 'xlarsx@gmail.com'
    },
    3: {
      name: 'Jesus Cagide',
      photo: '14298723.png',
      email: 'set311@gmail.com'
    }
  ]
  },
  notifications: {
    apu: 1,
    email: 1,
    reminder: 1,
    calendar: 1
  },
  chat: {
    1: {
      message: 'Hola!',
      email: 'fachinacg@gmail.com',
      id: 1,
      url: 'documento.doc'
    }
  }
}
```
