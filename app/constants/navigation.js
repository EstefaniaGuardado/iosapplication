'use strict'

const NAVBAR_COLOR = '#f5f5f5'

module.exports = {
  navbarColor : NAVBAR_COLOR,

  statusBarStyle : {
    tintColor : NAVBAR_COLOR,
    hidden    : false,
    style     : "default"
  }
}
