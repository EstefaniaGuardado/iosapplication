'use strict';

const React = require('react-native')
const ReactFireMixin = require('reactfire')
var Firebase = require("firebase")

const MeetingsTableView = require('./artifacts/MeetingsTableView')

const MeetingDetailViewController = require('./MeetingDetailViewController')

const { navbarColor, statusBarStyle } = require('../../constants/navigation')

const {
  AppRegistry,
  View,
  Image,
  StyleSheet
} = React;

const MeetingsListViewController = React.createClass({

  mixins: [ReactFireMixin],

  getInitialState : function() {
    return {
      listMeetingsInformation : []
    }
  },

  componentWillMount : function() {
    const ref = new Firebase("https://fiery-fire-7264.firebaseio.com/Users/84DFC119-D29B-44AE-B8E8-257DE184279A/meeting")
    this.bindAsArray(ref, "listMeetingsInformation")
  },

  selectedMeetingHandler: function(meeting) {
    this.props.navigator.push({
      title: meeting.name,
      component: MeetingDetailViewController,
      rightButtonIcon: { uri: 'chat'},
      passProps : { selectedMeeting : meeting }
    })
  },

  render: function() {
    const {listMeetingsInformation} = this.state

    let component
    if(listMeetingsInformation.length) {
      component = (
        <MeetingsTableView
          meetings={listMeetingsInformation}
          meetingHandler={this.selectedMeetingHandler}/>
      )
    } else {
      component = (
        <Image
          style={styles.image}
          source={{uri: '/Users/estefaniachavezguardado/Downloads/inicio.png'}}
        />
      )
    }

    return (
      <View>
        {component}
      </View>
    );
  }
});

var styles = StyleSheet.create({
  image:{
    flex: 1
  }
});

module.exports = MeetingsListViewController
