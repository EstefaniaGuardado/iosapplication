'use strict'

const React = require('react-native')
const TableView = require('react-native-tableview')
const moment = require('moment')

const {Section, Item, Cell} = TableView

const { StyleSheet, Text, View } = React

const viewModel = [
  {
    title : 'Date:',
    key : (meeting) => moment(meeting.detail.date).format('LL')
  },
  {
    title : 'Time:',
    key : (meeting) => moment(meeting.detail.date).format('HH:mm')
  }
]

const MeetingDetailInformation = React.createClass({

  getMeetingInformation: function() {
    const { meeting } = this.props

    if(meeting.detail) {
      return viewModel.map(function(viewModelComponent) {
        return (
          <Cell style={styles.cellMeeting}>
            <Text style={styles.detailCell}> {viewModelComponent.title}  {viewModelComponent.key(meeting)}</Text>
          </Cell>
        );
      });
    }
    return null
  },

  render() {
    const meetingInformation = this.getMeetingInformation()

    return (
      <View style={styles.detailInformation}>
        <Text style={styles.sectionName}>Information</Text>

        <TableView style={{flex:1}}
          tableViewStyle={TableView.Consts.Style.Plain}
          separatorColor="clear"
          tableViewCellStyle={TableView.Consts.CellStyle.Subtitle}>

          <Section arrow={false}>
            {meetingInformation}
          </Section>
        </TableView>
      </View>
    )
  }
})

const styles = StyleSheet.create({
  detailInformation : {
    flex : 1
  },
  cellMeeting: {
    height: 40,
    justifyContent: 'flex-start',
    flexDirection: 'row',
  },
  detailCell:{
    marginLeft: 20,
    fontSize: 12,
    fontFamily: 'Georgia',
    alignSelf: 'center',
  },
  sectionName:{
    padding: 8,
    fontSize: 15,
    color: '#00679a',
    alignSelf: 'center',
    //fontFamily: 'Georgia',
    //backgroundColor: '#f0f0f0'
  },
  notificationView:{
    flex: 1,
    flexDirection: 'row',
    padding: 5,
  },
  button: {
    height: 30,
    flex: 1,
    backgroundColor: '#e4e4e4',
    borderColor: '#e4e4e4',
    borderWidth: 0.5,
    borderRadius: 50,
    margin: 5,
    alignSelf: 'flex-end',
    justifyContent: 'center'
  },
  buttonText: {
    height: 20,
    width: 20,
    alignSelf: 'center'
  },
  optionsNotification:{
    fontSize: 15,
    color: '#00679a',
    alignSelf: 'center'
  },
  imageGuest:{
    height: 40,
    width: 40,
    borderColor: '#00679a',
    borderWidth: 1,
    borderRadius: 20,
    alignSelf: 'center',
    marginLeft: 10
  }
});


module.exports = MeetingDetailInformation
