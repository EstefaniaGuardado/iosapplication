'use strict'

const React = require('react-native')
const TableView = require('react-native-tableview')

const {Section, Item, Cell} = TableView

const { StyleSheet, Text, Image, View } = React

const MeetingDetailGuests = React.createClass({

  getGuestsMeetingList: function() {
    const { meeting } = this.props;

    if(meeting.guests) {
      return meeting.guests.map(function(guestsMeeting) {
        return (
          <Cell style={styles.cellMeeting}>
            <Image style={styles.imageGuest} source = {{uri: 'contact'}}/>
            <Text style={styles.detailCell}>{guestsMeeting.name}</Text>
          </Cell>
        );
      });
    }
    return null
  },

  render() {
    const guestsMeeting = this.getGuestsMeetingList()

    return (
      <View style={styles.detailGuess}>
        <Text style={styles.sectionName}>Guests</Text>
        <TableView style={{flex:1}}
            tableViewStyle={TableView.Consts.Style.Plain}
            separatorColor="clear"
            tableViewCellStyle={TableView.Consts.CellStyle.Subtitle}>
          <Section arrow={false}>
            {guestsMeeting}
          </Section>
        </TableView>
      </View>
    )
  }
})

const styles = StyleSheet.create({
  detailGuess : {
    flex : 3
  },
  cellMeeting: {
    height: 50,
    justifyContent: 'flex-start',
    flexDirection: 'row',
  },
  detailCell:{
    marginLeft: 20,
    fontSize: 12,
    fontFamily: 'Georgia',
    alignSelf: 'center',
  },
  sectionName:{
    padding: 8,
    fontSize: 15,
    color: '#00679a',
    alignSelf: 'center',
    //fontFamily: 'Georgia',
    //backgroundColor: '#f0f0f0'
  },
  notificationView:{
    flex: 1,
    flexDirection: 'row',
    padding: 5,
  },
  button: {
    height: 30,
    flex: 1,
    backgroundColor: '#e4e4e4',
    borderColor: '#e4e4e4',
    borderWidth: 0.5,
    borderRadius: 50,
    margin: 5,
    alignSelf: 'flex-end',
    justifyContent: 'center'
  },
  buttonText: {
    height: 20,
    width: 20,
    alignSelf: 'center'
  },
  optionsNotification:{
    fontSize: 15,
    color: '#00679a',
    alignSelf: 'center'
  },
  imageGuest:{
    height: 40,
    width: 40,
    borderColor: '#00679a',
    borderWidth: 1,
    borderRadius: 20,
    alignSelf: 'center',
    marginLeft: 10
  }
});


module.exports = MeetingDetailGuests
