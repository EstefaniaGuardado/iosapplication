'use strict'

const React = require('react-native')

const { StyleSheet, View, Text, TouchableHighlight, Image } = React

const viewModel = [
  {
    name : 'Push Notification',
    uri : 'pushOnPress'
  },
  {
    name : 'Calendar',
    uri : 'calendarOffPress'
  },
  {
    name : 'Reminders',
    uri : 'reminderOffPress'
  },
  {
    name : 'E-Mail',
    uri : 'emailOffPress'
  }
]

const MeetingDetailNotificationOptions = React.createClass({
  render() {

    const remindersList = viewModel.map(function(viewModelComponent) {
      return (
        <TouchableHighlight style={styles.buttonNotification} underlayColor='#ababab'>
          <Image style={styles.contentButton} source = {viewModelComponent} />
        </TouchableHighlight>
      )
    })

    return (
      <View style = {styles.viewOptionsNotification}>
        <Text style = {styles.titleOptionsNotification}>Options Notification</Text>
        <View style={styles.viewButtonNotifications}>
          {remindersList}
        </View>
      </View>
    )
  }
})

const styles = StyleSheet.create({
  viewOptionsNotification:{
    flex : 1
  },
  titleOptionsNotification:{
    fontSize: 15,
    color: '#00679a',
    alignSelf: 'center'
  },
  viewButtonNotifications:{
    flexDirection: 'row'
  },
  buttonNotification: {
    height: 40,
    flex: 1,
    backgroundColor: '#e4e4e4',
    borderColor: '#e4e4e4',
    borderWidth: 0.5,
    borderRadius: 50,
    margin: 20,
    justifyContent: 'center'
  },
  contentButton: {
    height: 20,
    width: 20,
    alignSelf: 'center'
  }
});


module.exports = MeetingDetailNotificationOptions
