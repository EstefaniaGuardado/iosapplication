'use strict';

const React = require('react-native');
const TableView = require('react-native-tableview')
const {Section, Item, Cell} = TableView
const moment = require('moment')

const {
  StyleSheet,
  Text,
  View
} = React;

const MeetingsTableView = React.createClass({
  getInitialState : function() {
    return {
      meetingGroupe : "List of Meetings"
    };
  },

  meetingSelected: function(event) {
    const { meetingHandler, meetings } = this.props

    const meetingToSay = meetings[event.selectedIndex]

    meetingHandler(meetingToSay)
  },

  getListMeetings: function() {
    const { meetings } = this.props;

    return meetings.map(function(meeting) {
      return (
        <Cell style={styles.cellMeeting}>
          <Text>{meeting.name}</Text>
          <Text style={styles.detailCell}>{moment(meeting.date).format('LL')}</Text>
          <Text style={styles.detailCell}>{moment(meeting.date).format('HH:mm')}</Text>
        </Cell>
      );
    });
  },

  render() {
    const { meetingGroupe } = this.state;
    const listMeetings = this.getListMeetings()

    return (
      <View>
        <TableView style={styles.listMeetings}
          tableViewStyle={TableView.Consts.Style.Plain}
          tableViewCellStyle={TableView.Consts.CellStyle.Subtitle}
          onPress={this.meetingSelected}>
          <Section arrow={true}>
            {listMeetings}
          </Section>
        </TableView>
    </View>
    )
  }
})

const styles = StyleSheet.create({
  listMeetings: {
    flex:1,
    marginTop: 21,
    height: 200
  },
  cellMeeting: {
    height: 30,
    justifyContent: 'center',
    padding:30
  },
  detailCell:{
    fontSize: 12,
    // fontFamily: 'Georgia',
  },
})


module.exports = MeetingsTableView
