'use strict'

const React = require('react-native')
const ReactFireMixin = require('reactfire')
const TableView = require('react-native-tableview')

const {Section, Item, Cell} = TableView

const { StyleSheet, View, Text, TouchableHighlight, Image } = React

const { navbarColor, statusBarStyle } = require('../../constants/navigation')

const MeetingDetailNotificationOptions = require('./artifacts/MeetingDetailNotificationOptions'),
      MeetingDetailInformation = require('./artifacts/MeetingDetailInformation'),
      MeetingDetailGuests = require('./artifacts/MeetingDetailGuests')

const FIREBASE_URL = 'https://fiery-fire-7264.firebaseio.com/Meetings/'

const MeetingDetailViewController = React.createClass({

  mixins: [ReactFireMixin],

  getInitialState : function() {
    return {
      detailMeetingInformation : {}
    }
  },

  componentWillMount : function() {
    const { selectedMeeting } = this.props
    const ref = new Firebase(FIREBASE_URL + selectedMeeting.meetingId)
    this.bindAsObject(ref, "detailMeetingInformation")
  },

  render() {
    let { detailMeetingInformation } = this.state

    return (
      <View style={styles.detailViewGeneral}>

        <MeetingDetailInformation meeting={detailMeetingInformation} />
        <MeetingDetailGuests meeting={detailMeetingInformation} />
        <MeetingDetailNotificationOptions style={styles.notificationView} meeting={detailMeetingInformation} />

      </View>
    )
  }
})

const styles = StyleSheet.create({
  detailViewGeneral: {
    flex: 1,
    backgroundColor: '#ffffff',
    marginTop: 70
  },
  notificationView:{
    flex: 1,
    height : 30,
    alignSelf: 'center'
  }
});


module.exports = MeetingDetailViewController
