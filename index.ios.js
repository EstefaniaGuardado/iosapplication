/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

const React = require('react-native');
const GuestsListViewController = require('./app/meetings/components/MeetingsListViewController.js')

const {
  AppRegistry,
  View,
  NavigatorIOS,
  StyleSheet,
  TouchableHighlight
} = React;

const iOSApplication = React.createClass({

  render:function(){
    return (
      <NavigatorIOS
        style={styles.navigatorBar}
        tintColor= '#5b009c'
        titleTextColor= '#5b009c'
        initialRoute={{
          title: 'Meetings',
          rightButtonIcon: { uri: 'add'},
          leftButtonIcon: { uri: 'menu'},
          component: GuestsListViewController
        }}
      />
    )
  }
})

var styles = StyleSheet.create({
  navigatorBar:{
    flex: 1,
  }
})

AppRegistry.registerComponent('iOSApplication', () => iOSApplication)
